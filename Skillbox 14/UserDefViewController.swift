//
//  UserDefViewController.swift
//  Skillbox 14
//
//  Created by electra on 23.11.2019.
//  Copyright © 2019 electra. All rights reserved.
//

import UIKit

class UserDefViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    
    @IBOutlet weak var secondNameTextField: UITextField!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var secondNameLabel: UILabel!
    
    @IBAction func actionName(_ sender: Any) {
        name = nameTextField.text!
    }
    
    @IBAction func actionSecondName(_ sender: Any) {
         secondName = secondNameTextField.text!
    }
    
    var name = "" {
        didSet {
            UserDefaults.standard.set(name, forKey: "name")
            nameLabel.text = UserDefaults.standard.string(forKey: "name")
        }
    }
    var secondName = "" {
        didSet {
            UserDefaults.standard.set(secondName, forKey: "secondName")
            secondNameLabel.text = UserDefaults.standard.string(forKey: "secondName")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = UserDefaults.standard.string(forKey: "name")
        secondNameLabel.text = UserDefaults.standard.string(forKey: "secondName")
    }
}
