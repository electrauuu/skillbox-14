//
//  CoreDataViewController.swift
//  Skillbox 14
//
//  Created by electra on 24.11.2019.
//  Copyright © 2019 electra. All rights reserved.
//

import UIKit
import CoreData


class CoreDataViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var tasks: [NSManagedObject] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    var textLabelText = ""
    var viewIsHidden = true
    
    @IBOutlet weak var toDoTextField: UITextField!
    
    @IBOutlet weak var addView: UIView!
    
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var addButton: UIButton!
    
    @IBAction func resetButtonAction(_ sender: Any) {
        textLabelText = ""
        if viewIsHidden {viewIsHidden = false }
        else {viewIsHidden = true}
        addView.isHidden = viewIsHidden
        toDoTextField.text = ""
    }
    
    @IBAction func addTaskAction(_ sender: Any) {
        if toDoTextField.text != "" {
            
            let nameToSave = toDoTextField.text!
            self.save(name: nameToSave)
            
            if viewIsHidden {viewIsHidden = false }
            else {viewIsHidden = true}
            addView.isHidden = viewIsHidden
            toDoTextField.text = ""
        }
    }

    @IBAction func textFieldAction(_ sender: Any) {
        textLabelText = toDoTextField.text!
    }
    
    @IBAction func addNewTask(_ sender: Any) {
        if viewIsHidden {viewIsHidden = false }
        else {viewIsHidden = true}
        addView.isHidden = viewIsHidden
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addView.isHidden = viewIsHidden
        button.layer.cornerRadius = 15
        let w = view.frame.size.width
        addView.frame = CGRect(x: (w / 10 ) / 2, y: 300, width: w - w / 10, height: 150)
        addButton.layer.cornerRadius = 10
        resetButton.layer.cornerRadius = 10
        addView.layer.cornerRadius = 20
    }
    
    override func viewWillAppear(_ animated: Bool) {
      super.viewWillAppear(animated)
      
      guard let appDelegate =
        UIApplication.shared.delegate as? AppDelegate else {
          return
      }
      
      let managedContext =
        appDelegate.persistentContainer.viewContext
      
      let fetchRequest =
        NSFetchRequest<NSManagedObject>(entityName: "Task")
      
      do {
        let people = try managedContext.fetch(fetchRequest)
        tasks = people
      } catch let error as NSError {
        print("Could not fetch. \(error), \(error.userInfo)")
      }
    }

    
    func save(name: String) {
      
      guard let appDelegate =
        UIApplication.shared.delegate as? AppDelegate else {
        return
      }
      
      let managedContext =
        appDelegate.persistentContainer.viewContext
      
      let entity =
        NSEntityDescription.entity(forEntityName: "Task",
                                   in: managedContext)!
      
      let task = NSManagedObject(entity: entity,
                                   insertInto: managedContext)
      
      task.setValue(name, forKeyPath: "taskName")
      
      do {
        try managedContext.save()
        tasks.append(task)
      } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
      }
    }
    
}
extension CoreDataViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let task = tasks[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "CoreDataTableViewCell") as! CoreDataTableViewCell
        cell.taskLabel.text = task.value(forKeyPath: "taskName") as? String
        return cell
    }
}
