//
//  toDoListViewController.swift
//  Skillbox 14
//
//  Created by electra on 23.11.2019.
//  Copyright © 2019 electra. All rights reserved.
//

import UIKit
import RealmSwift

class TaskRealm: Object {
    @objc dynamic var taskNameRealm = ""
}

class toDoListViewController: UIViewController {

    private let realm = try! Realm()
    
    var viewIsHidden = true
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var toDoTextField: UITextField!
    
    @IBOutlet weak var addView: UIView!
    
    @IBOutlet weak var button: UIButton!
    
    @IBOutlet weak var resetButton: UIButton!
    
    @IBOutlet weak var addButton: UIButton!

    @IBAction func resetButtonaction(_ sender: Any) {
        textLabelText = ""
        if viewIsHidden {viewIsHidden = false }
        else {viewIsHidden = true}
        addView.isHidden = viewIsHidden
        toDoTextField.text = ""
    }
    @IBAction func addTaskAction(_ sender: Any) {
        if toDoTextField.text != "" {
            arrayOfTasks.append(toDoTextField.text!)
            
            let newTask = TaskRealm()
            newTask.taskNameRealm = toDoTextField.text!
            try! realm.write {
                realm.add(newTask)
            }
            
            if viewIsHidden {viewIsHidden = false }
            else {viewIsHidden = true}
            addView.isHidden = viewIsHidden
            toDoTextField.text = ""
        }
    }
    
    @IBAction func TextFieldAction(_ sender: Any) {
        textLabelText = toDoTextField.text!
    }
    
    @IBAction func addNewTask(_ sender: Any) {
        if viewIsHidden {viewIsHidden = false }
        else {viewIsHidden = true}
        addView.isHidden = viewIsHidden
        
    }
    
    var arrayOfTasks = [String]() {
        didSet {
            tableView.reloadData()
        }
    }
    var textLabelText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addView.isHidden = viewIsHidden
        button.layer.cornerRadius = 15
        let w = view.frame.size.width
        addView.frame = CGRect(x: (w / 10 ) / 2, y: 300, width: w - w / 10, height: 150)
        addButton.layer.cornerRadius = 10
        resetButton.layer.cornerRadius = 10
        addView.layer.cornerRadius = 20
        
        for task in realm.objects(TaskRealm.self) {
            arrayOfTasks.append(task.taskNameRealm)
        }
    }
}
extension toDoListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfTasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "toDoTableViewCell") as! toDoTableViewCell
        cell.taskLabel.text = arrayOfTasks[indexPath.row]
        return cell
    }
}
