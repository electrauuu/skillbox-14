//
//  toDoTableViewCell.swift
//  Skillbox 14
//
//  Created by electra on 23.11.2019.
//  Copyright © 2019 electra. All rights reserved.
//

import UIKit

class toDoTableViewCell: UITableViewCell {

    @IBOutlet weak var taskLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
